from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello world!")


def end_lesson(request):
    return HttpResponse("Lesson stopped! Good evening!")
